import com.sun.org.apache.xpath.internal.operations.Bool
import java.io.File
//import java.io.*

//import javax.print.attribute.standard.JobHoldUntil
import kotlin.io.*

class Game {

    val myBoard = Board()
    val enemyBoard = Board()
    var status: String = GAME_STATUS_ONGOING
    val filename = "resources/ShipPlacement.txt"
    val enemyFilename = "resources/enemyShipPlacement.txt"
    var HitOrMiss: Boolean = false
//    var strlist = listOf<String>()
//    var enemyStrlist = listOf<String>()

    fun readFileToList(fileName: String): List<String> = File(fileName).readLines()
    val strlist = readFileToList(filename).toMutableList()
    val enemystrlist = readFileToList(enemyFilename).toMutableList()

    fun start() {
        println("Now placing ships for player number one\n")
        while (myBoard.ships.count() != GAME_MAX_NUMBER_OF_SHIPS) {
            placeShipOnBoard(myBoard, strlist)
        }
        println("Now placing ships for player number two\n")
        while (enemyBoard.ships.count() != GAME_MAX_NUMBER_OF_SHIPS) {
            placeShipOnBoard(enemyBoard, enemystrlist)
        }

        while (status == GAME_STATUS_ONGOING) {
            myTurn()
            while (HitOrMiss == true) myTurn()
            enemyTurn()
            while (HitOrMiss == true) enemyTurn();
        }

        endGameMessage()
    }

    fun printBoards() {
        println("myboard")
        for (i in BOARD_X_MIN..BOARD_X_MAX) {
            for( j in BOARD_Y_MIN..BOARD_Y_MAX) {
                for (ship in myBoard.ships) {
                    for(cell in ship.cells){
                        if(cell.equalsTo(Cell(i,j))) {
                            print(" S ")
                        } else {
                            print(" | ")
                        }
                    }
                }
            }
        }
        println("enemyBoard")
        for (i in BOARD_X_MIN..BOARD_X_MAX) {
            for( j in BOARD_Y_MIN..BOARD_Y_MAX) {
                for (ship in enemyBoard.ships) {
                    for(cell in ship.cells){
                        if(cell.equalsTo(Cell(i,j))) {
                            print(" S ")
                        } else {
                            print(" | ")
                        }
                    }
                }
            }
        }
    }

    private fun placeShipOnBoard(holdingBoard: Board, strlist: MutableList<String>) {
        println("Please enter ship size ")
        val ship = Ship(strlist.get(0).toInt())
        println(strlist.get(0).toInt())
        strlist.removeAt(0)
//        val ship = Ship(readLine()!!.toInt())
//        val ship = Ship(inputStream.bufferedReader().readLine().toInt())
        println("Please enter ship head coordinates and its direction ")
//        val (x: String, y: String, direction: String) = readLine()!!.split(" ")
//        val(x:String, y:String,direction:String) = inputStream.bufferedReader().readLine()!!.split(" ")
        val (x: String, y: String, direction: String) = strlist[0].split(" ")
        print(" x = $x y= $y direction = $direction")
        strlist.removeAt(0)

        ship.make(Cell(x.toInt(), y.toInt()), direction)
        val check = holdingBoard.ships.count()
        holdingBoard.addShip(ship)
//        println(check)
//        println(holdingBoard.count)
        println(holdingBoard.ships.count())
        if (holdingBoard.ships.count() == check)
            println(GAME_SHIP_NOT_ADDED)
    }


    private fun myTurn() {
        print("First player fires\n")

        printBoards();

        val (x: String, y: String) = readLine()!!.split(" ")
        HitOrMiss = enemyBoard.fire(Cell(x.toInt(), y.toInt()))

        //Удар по клетке которую выбрал враг
        //Выбрать клетку по которой атакуем мы
    }

    private fun enemyTurn() {
        print("Second player fires\n")
        val (x: String, y: String) = readLine()!!.split(" ")
        HitOrMiss = myBoard.fire(Cell(x.toInt(), y.toInt()))
        //Получаем ответочку от врага
        //Не сдох ли он уже
        //Если нет то какую клетку атакует
        //Какую клетку он атакует
        //Отправляем врагу клетку которую атакуем
    }

    private fun checkGameStatus() {
        if (myBoard.ships.count { it.status == SHIP_STATUS_DEAD } == 0)
            status = GAME_STATUS_LOST
    }

    private fun endGameMessage() {
        when (status) {
            GAME_STATUS_WIN -> println(GAME_WIN_MESSAGE)
            GAME_STATUS_LOST -> println(GAME_LOSE_MESSAGE)
            GAME_STATUS_ONGOING -> println(GAME_ONGOING_ERROR_MESSAGE)
        }
    }

}